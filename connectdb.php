<?php 
function konek($host, $username, $password, $schemaname = '160419017_dbase_uas') {
    mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);

    $mysqli = new mysqli($host, $username, $password, $schemaname);

    $mysqli->set_charset('utf8mb4');

    return $mysqli;
}
?>