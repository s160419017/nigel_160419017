<?php
include ('connectdb.php');
$mysqli = konek('159.65.0.229', 'root', 'mysql4our');
$sql = "SELECT * from biodata";
    $stmt = $mysqli->prepare($sql);
    $stmt->execute();
    $res = $stmt->get_result();
?>
<!DOCTYPE HTML>
<!--
	Solid State by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<title>Solid State by HTML5 UP</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<link rel="stylesheet" href="main.css" />
		<noscript><link rel="stylesheet" href="noscript.css" /></noscript>
	</head>
	<body class="is-preload">

		<!-- Page Wrapper -->
			<div id="page-wrapper">

				<!-- Header -->
					<header id="header" class="alt">
						<h1><a href="index.html">Solid State</a></h1>
						<nav>
							<a href="#menu">Menu</a>
						</nav>
					</header>

				<!-- Menu -->
					<nav id="menu">
						<div class="inner">
							<h2>Menu</h2>
							<ul class="links">
								<li><a href="index.html">Home</a></li>
								<li><a href="generic.html">Generic</a></li>
								<li><a href="elements.html">Elements</a></li>
								<li><a href="#">Log In</a></li>
								<li><a href="#">Sign Up</a></li>
							</ul>
							<a href="#" class="close">Close</a>
						</div>
					</nav>

				<!-- Banner -->
					<section id="banner">
						<div class="inner">
							<div class="logo"><span class="icon fa-gem"></span></div>
							<h2>Nigel Kislew William_160419017</h2>
							<h3>Biodata</h3>
							<p><a href="http://html5up.net">Template diambil dari</a></p>
						</div>
					</section>

				<!-- Wrapper -->
					<section id="wrapper">

						<!-- One -->
							<section id="one" class="wrapper spotlight style1">
								<div class="inner">
									<a href="#" class="image"><img src="pic01.jpg" alt="" /></a>
									<div class="content">
										<h2 class="major">ID</h2>
										<p><?php
										while($row = $res->fetch_assoc())
										{
										echo $row['id'];										
										}
										?></p>
										<a href="#" class="special">Learn more</a>
									</div>
								</div>
							</section>

						<!-- Two -->
							<section id="two" class="wrapper alt spotlight style2">
								<div class="inner">
									<a href="#" class="image"><img src="pic02.jpg" alt="" /></a>
									<div class="content">
										<h2 class="major">NRP</h2>
										<p><?php
										while($row = $res->fetch_assoc())
										{
										echo $row['nrp'];										
										}
										?></p>
										<a href="#" class="special">Learn more</a>
									</div>
								</div>
							</section>

						<!-- Three -->
							<section id="three" class="wrapper spotlight style3">
								<div class="inner">
									<a href="#" class="image"><img src="pic03.jpg" alt="" /></a>
									<div class="content">
										<h2 class="major">Nama</h2>
										<p><?php
										while($row = $res->fetch_assoc())
										{
										echo $row['nama'];										
										}
										?></p>
										<a href="#" class="special">Learn more</a>
									</div>
								</div>
							</section>

						<!-- Four -->
							<section id="four" class="wrapper alt style1">
								<div class="inner">
									<h2 class="major">Alamat</h2>
									<p><?php
										while($row = $res->fetch_assoc())
										{
										echo $row['alamat'];										
										}
										?></p>
									<h2 class="major">Kota</h2>
									<p><?php
										while($row = $res->fetch_assoc())
										{
										echo $row['kota'];										
										}
										?></p>
																		
								</div>
							</section>

					</section>				

			</div>

		<!-- Scripts -->
			<script src="jquery.min.js"></script>
			<script src="jquery.scrollex.min.js"></script>
			<script src="browser.min.js"></script>
			<script src="breakpoints.min.js"></script>
			<script src="util.js"></script>
			<script src="main.js"></script>

	</body>
</html>